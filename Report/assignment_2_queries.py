import re
import rdflib
from rdflib import URIRef

def get_student_knowledge_details(student, g):
    q5 = g.query(
        f""" SELECT DISTINCT ?topic
        WHERE {{
        ?stu rdf:type people:Student .
        ?stu foaf:givenName ?stuname .
        ?stu property:hasRecord ?record .
        
        {{
        SELECT DISTINCT ?topic
        WHERE {{
        ?record property:Course ?stucourse .
        ?stucourse foaf:topic ?topicuri .
        ?topicuri rdf:type course:Topic .
        ?topicuri foaf:name ?topic .
        ?record property:Grade ?grade .
        FILTER (?grade != 'F'@en)
        }}
        }}
        
        FILTER (regex(str(?stuname),'{student}','i'))
        
        }}ORDER BY asc(?topic)
        """)
    
    if q5 is None or len(q5) == 0:
        print("%s has no knowledge on course topics"%student)
    
    else:
        print("List of Topics %s knows" % student)
        print("------------------------------------------------------------------------")

    for row in q5:
        print("%s" % row)

def get_student_details_familiar_with_topic(topic_name, g):
    q4=g.query(
        f""" SELECT ?studentname ?course ?coursename
        WHERE {{
        ?topicuri rdf:type course:Topic .
        ?topicuri foaf:name ?topic .
        ?topicuri property:partOf ?course .
        ?course rdf:type course:Course .
        ?course property:CourseName ?coursename .
                
        {{
        SELECT ?stucourse ?studentname
        WHERE{{
        ?stu rdf:type people:Student .
        ?stu property:hasRecord ?record .
        ?stu foaf:givenName ?studentname .
        
        {{
        SELECT ?stucourse
        WHERE{{
        ?record property:Course ?stucourse .
        ?record property:Grade ?grade .
        FILTER (?grade != 'F'@en)
        }}
        }}

        }}
                        
        }}
        FILTER(regex(str(?topic),'{topic_name}','i') && (?course = ?stucourse))
        
        }}
        """)
    
    if q4 is None or len(q4) == 0:
        print("No students found with knowledge on %s"%topic_name)
    else:
        print("List of Students who has knowledge on " +topic_name)
        print("Student Name\t\tCourse ID\t\t\t\tCourse Name")
        print("-----------------------------------------------------------------------------------")

    for row in q4:
        print("%s\t\t\t%s\t\t\t%s" % row)

def get_courses_with_topic(topic_name, g):
    q3 = g.query(
        f""" SELECT ?courseSub ?courseNum ?coursename
        WHERE {{
        ?topicuri rdf:type course:Topic .
        ?topicuri foaf:name ?topic .
        ?topicuri property:partOf ?course .
        ?course rdf:type course:Course .
        ?course property:CourseName ?coursename .
        ?course property:CourseSubject ?courseSub .
        ?course property:CourseNumber ?courseNum .
        FILTER (regex(str(?topic),'{topic_name}','i'))
        }}
        """)
    if q3 is None or len(q3) == 0:
        print("No courses found with topic %s"%topic_name)
    else:
        print("Courses covering the %s" % topic_name)
    for row in q3:
        print("\t%s %s %s" % row)

def get_course_student_take(student, g):
    q2 = g.query(
        f""" SELECT ?grade ?semester ?courseSub ?courseNum ?coursename 
        WHERE {{
        ?stu rdf:type people:Student .
        ?stu foaf:givenName ?stuname .        
        ?stu property:hasRecord ?record .
        {{
        SELECT ?courseSub ?courseNum ?coursename ?grade ?semester
        WHERE{{
        ?record property:Course ?courseuri .
        ?courseuri property:CourseSubject ?courseSub .
        ?courseuri property:CourseNumber ?courseNum .
        ?courseuri property:CourseName ?coursename .
        FILTER(lang(?coursename)='en')
        ?record property:Grade ?grade .
        ?record property:Semester ?semester .
        }}
        }}
        FILTER (regex(str(?stuname),'{student}','i'))
        }}
        """)
    
    if q2 is None or len(q2) == 0:
        print("%s student didn't take any course"%student)
    else:
        print("Grade\t\tSemester\t\tCourseName")
        print("-----------------------------------------------------------------------------------------------------------")
    for row in q2:
        print("%s\t\t%s\t\t%s %s %s" % row)

def get_course_description(course_name, g):
    #print(course_name)
    #c_name = str(course_name)
    q1 = g.query(
            f"""SELECT ?info ?link
            WHERE {{
            ?c rdf:type course:Course .
            ?c foaf:name '{course_name}' .
            ?c dbo:abstract ?info . 
            ?c rdfs:seeAlso ?link .
            }}
        """)
    
    if q1 is None or len(q1) == 0:
        print("\n%s details hasn't been found"%course_name)
    for row in q1:
        print("\n%s details:" % course_name)
        print("\t%s\n\nFor more information on the course, go to the following link:\n%s" % row)
    
def get_result_of_question(ques, g):
    if(re.search("^what is the .*( about)?(/?)?", ques)):
        question_words = ques.split(' ')
        if((len(question_words) == 5 and (question_words[4] != 'about' and question_words[4] != 'about?')) or (len(question_words) == 6)):
            course_name = question_words[3]+question_words[4]
        else:
            course_name = question_words[3]
        get_course_description(course_name.upper(), g)
    if(re.match("^which courses did .* take?", ques)):
        matched_regex = re.match("^which courses did (.*) take?", ques)
        get_course_student_take(matched_regex.group(1).strip().replace(" ",""),g)
    if(re.match("^which courses cover .*?", ques)):
        matched_regex = re.match("^which courses cover (.*)?", ques)
        get_courses_with_topic(matched_regex.group(1),g)
    if(re.match("^who is familiar with .*?", ques)):
        matched_regex = re.match("^who is familiar with (.*)?", ques)
        get_student_details_familiar_with_topic(matched_regex.group(1), g)
    if(re.match("^what does .* know?", ques)):
        matched_regex = re.match("^what does (.*) know?", ques)
        get_student_knowledge_details(matched_regex.group(1).strip().replace(" ",""), g)
    

if __name__ == "__main__":
    g = rdflib.Graph()

    g.parse("G12.ttl", format="n3")
    ques = input("Hello, welcome to Concordia University assistant\nHow may I help you?\n")
    while True:
        if(ques.lower() == 'quit' or ques.lower() == 'q'):
            print('Thank you for using Concordia University assistant!!')
            break
        get_result_of_question(ques.lower(), g)
        ques = input("\nAnything else you would like to check?\n")
        