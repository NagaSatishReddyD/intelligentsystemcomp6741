# -*- coding: utf-8 -*-
"""
Created on Sat Feb 22 01:07:55 2020

@author: Reethu Navale
"""

import rdflib
from rdflib import URIRef

g = rdflib.Graph()

g.parse("G12.rdf", format="n3")

### This will give the number of triples in non query form
#print(len(g)) 

########### SPARQL query for triple count
q1 = g.query(
    """SELECT (COUNT (*) as ?CountTriples)
      WHERE {
          ?s ?p ?o .
       }""" )


for row in q1:
    print("Total number of triples in the RDF : %s" % row)

#######################################################################

q2 = g.query(
        """SELECT (COUNT(?m) as ?countStudent) ?countCourse ?countTopic
      WHERE {
          ?s rdf:type people:Student .
          ?s foaf:mbox ?m .
          {
          SELECT (COUNT(?cn) as ?countCourse)
          WHERE {
          ?c rdf:type course:AcademicSubject .
          ?c property:CourseNumber ?cn .
          } 
          }
          
          {
          SELECT (COUNT(?t) as ?countTopic)
          WHERE {
          ?c rdf:type course:AcademicSubject .
          ?c foaf:name ?t .
          } 
          }    
       }""")

for row in q2:
    print("Total number of students in the RDF : %s\nTotal number of courses in the RDF : %s\nTotal number of Topics in the RDF : %s" % row)

######################################################################


coursename = str('ASEM651')
print("Topics for under the course :" + coursename)
q3 = g.query(
        f"""SELECT DISTINCT ?t
        WHERE {{
        ?c rdf:type course:AcademicSubject .
        ?c foaf:name '{coursename}' .
        ?c foaf:name ?t .
        }}
        """)

for row in q3:
    print("\t%s" % row)

######################################################################

studentId = '40104630'
print("Student Id :" + studentId +"\nList of the courses :")
q4 = g.query(
        f""" SELECT ?firstname ?course ?grade ?semester
        WHERE {{
        ?stu rdf:type people:Student .
        ?stu people:student_id '{studentId}' .
        ?stu foaf:givenName ?firstname .
        ?stu property:hasRecord ?record .
        {{
        SELECT ?course ?grade ?semester
        WHERE{{
        ?record property:Course ?courseuri .
        ?courseuri foaf:name ?course .
        ?record property:Grade ?grade .
        ?record property:Semester ?semester .
        }}
        }}
        
        }}
        """)

for row in q4:
    print("Student name : %s\nCourse : %s\nGrade : %s\nSemester : %s" % row)



studentName = 'NagaSatish'
print("Student Name :" + studentName)
q4a = g.query(
        f""" SELECT ?course ?grade ?semester
        WHERE {{
        ?stu rdf:type people:Student .
        ?stu foaf:givenName '{studentName}'@en.        
        ?stu property:hasRecord ?record .
        {{
        SELECT ?course ?grade ?semester
        WHERE{{
        ?record property:Course ?courseuri .
        ?courseuri foaf:name ?course .
        ?record property:Grade ?grade .
        ?record property:Semester ?semester .
        }}
        }}
        
        }}
        """)

for row in q4a:
    print("Course : %s\nGrade : %s\nSemester : %s" % row)

######################################################################
##6231 course with topics
topic = 'Distributed Computing'
print("List of Students who has knowledge on " +topic)

# topicURI = str('http://dbpedia.org/resource/%s'%topic)
#print (topicURI)

q5=g.query(
        f""" SELECT ?course ?studentname
        WHERE {{
            ?topic rdf:type course:Topic .
            ?topic foaf:name ?tname .
            FILTER(regex(str(?tname), '{topic}', 'i')) 
        ?course foaf:name ?topic.
        ?course rdf:type course:AcademicSubject .
        {{
        SELECT ?stucourse ?studentname
        WHERE{{
        ?stu rdf:type people:Student .
        ?stu property:hasRecord ?record .
        ?stu foaf:givenName ?studentname .
        
        {{
        SELECT ?stucourse
        WHERE{{
        ?record property:Course ?stucourse .
        ?record property:Grade ?grade
        FILTER (?grade != 'F'@en)
        }}
        }}

        }}
                        
        }}
       
        FILTER (?course = ?stucourse)
        
        }}
        """)

for row in q5:
    print("Course : %s\nStudent : %s" % row)

######################################################################
studentIdq6 = '40104630'
print("List of Topics which student has knowledge on :")
q6= g.query(
        f""" SELECT DISTINCT ?studentname ?topic
        WHERE {{
        ?stu rdf:type people:Student .
        ?stu people:student_id '{studentIdq6}' .
        ?stu property:hasRecord ?record .
        ?stu foaf:givenName ?studentname .
        
        {{
        SELECT DISTINCT ?topic
        WHERE {{
        ?record property:Course ?stucourse .
        ?record property:Grade ?grade
        FILTER (?grade != 'F'@en)
        ?stucourse foaf:name ?topic
        }}
        }}
        
        
        }}
        """)

for row in q6:
    print("Student Name : %s\tTopic : %s" % row)