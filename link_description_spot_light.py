# -*- coding: utf-8 -*-
"""
Created on Thu Feb 13 16:54:24 2020

@author: Naga Satish Reddy
"""

import requests
import json
import csv

def main():
    sample_description = 'Introduction to discrete-event systems (DES). Modelling (languages, automata and Petri nets). Supervisory control (controllability, modular control and control under partial observation). Architecture (decentralized and hierarchical schemes). Petri nets (modelling and analysis). Timed models. A project is required.'
    spot_light_url = "https://api.dbpedia-spotlight.org/en/annotate?text=%s"%sample_description
    headers = {
        'accept': 'application/json'
    }
    r = requests.get(url=spot_light_url, headers=headers)
    data = r.json()
    print(data)
    for k in data['Resources']:
        print(k['@URI'])
        
def get_links_from_file():
    with open("description_data.json", "r") as file:
        json_data = json.load(file)
    courseNames = list(json_data.keys())
    # print(courseNames)
    
    with open("links.csv", "w") as csvFile:
        filewriter = csv.writer(csvFile, delimiter=',',
                            quotechar='"', quoting=csv.QUOTE_MINIMAL)
        for courseName in courseNames:
            data = json_data[courseName]
            # print(data)
            for k in data:
                filewriter.writerow([str(courseName), str(k['@URI'])])
    
if __name__ == '__main__':
    # main()
    get_links_from_file()