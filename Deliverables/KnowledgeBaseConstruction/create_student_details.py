# -*- coding: utf-8 -*-
"""
Created on Wed Feb 12 16:14:12 2020

@author: Naga Satish Reddy
"""

from rdflib import Graph, Literal, Namespace, RDFS, URIRef
from rdflib.namespace import RDF, FOAF

import csv
import requests
import time
import json
import pathlib
from random import randrange

stu = Namespace('http://focu.io/data/people/')
university_name_space = Namespace('http://focu.io/data/university/')
course_name_space = Namespace('http://focu.io/data/course/')
property_name_space = Namespace('http://focu.io/data/property/')
dbpedia =  Namespace('http://dbpedia.org/resource/')
dbo = Namespace('http://dbpedia.org/ontology/')
owl = Namespace('http://www.w3.org/2002/07/owl#')

def add_courses_to_graph(data, graph, student, student_id):
    # print(len(data))
    no_of_courses = int(len(data)/3)
    courses = data[0: no_of_courses]
    grades = data[no_of_courses:  2* no_of_courses]
    term = data[2*no_of_courses:]
    
    for course, grade, term in zip(courses, grades, term):
        student_record = stu["%s%s%s"%(student_id, course, term.replace(' ', ''))]
        graph.add((student, property_name_space['hasRecord'],student_record))
        graph.add((student_record, property_name_space['Course'], course_name_space[course]))
        graph.add((student_record, property_name_space['Semester'], Literal(term, lang='en')))
        graph.add((student_record, property_name_space['Grade'], Literal(grade, lang='en')))
        #graph.add((course_name_space[course], property_name_space[student_id], Literal("%s_%s"%(grade, term), lang='en')))
    
    # print(courses, grades)

def parse_student_add_to_graph(data, graph):
    student = stu[data[2]]
    graph.bind('foaf', FOAF)
    graph.bind('people', stu)
    graph.bind('property', property_name_space)
    graph.bind('course', course_name_space)
    graph.bind('dbo', dbo)
    graph.bind('dbpedia', dbpedia)
    graph.bind('owl', owl)
    graph.bind('uni', university_name_space)
    graph.add((student, RDF.type, stu.Student))
    graph.add((student, FOAF.familyName, Literal(data[1], lang='en')))
    graph.add((student, FOAF.givenName, Literal(data[0], lang='en')))
    graph.add((student, property_name_space.student_id, Literal(data[2])))
    graph.add((student, FOAF.mbox, Literal(data[3])))
    graph.add((student, RDFS.label, Literal("%s_%s"%(data[1], data[0]), lang='en')))
    graph.add((student, RDFS.comment, Literal("Student studies at an university", lang='en')))
    graph.add((student, property_name_space.studiesAt, university_name_space.ConcordiaUniversity))
    graph.add((student, property_name_space.isStudentOf, university_name_space.ConcordiaUniversity))
    graph.add((university_name_space.ConcordiaUniversity, property_name_space.hasStudent, student))

    add_courses_to_graph(data[4:], graph, student, data[2])
    
    
def add_description_to_dbpedia(graph, course, description, json_data, courseName):
    spot_light_url = "https://api.dbpedia-spotlight.org/en/annotate?text=%s"%description
    headers = {
            'accept': 'application/json'
            }
    if courseName in json_data:
        data = json_data[courseName]
        #print("came from file : %s"%courseName)
    else:
        try:
            r = requests.get(url=spot_light_url, headers=headers)
            print("Fetching from spotlight%s"%courseName)
        except:
            print("Links getting error for :%s"%courseName)
            time.sleep(randrange(1, 5))
            r = requests.get(url=spot_light_url, headers=headers)
        try:    
            data = r.json()
            #json_data[courseName] = data
            if('Resources' in data):
                json_data[courseName] = data['Resources']
            else:
                json_data[courseName] = []
        except:
            print("Json error for :%s"%courseName)
    try:
        #if('Resources' in data):
        #for k in data['Resources']:
        for k in data:
            link_name = k['@URI'][k['@URI'].rindex('/')+1:]
            graph.add((course, FOAF.topic, course_name_space[link_name]))
            graph.add((course_name_space[link_name], RDF.type, course_name_space.Topic))
            graph.add((course_name_space[link_name], owl.sameAs, URIRef(k['@URI'])))
            graph.add((course_name_space[link_name], RDF.type, course_name_space.Topic))
            graph.add((course_name_space[link_name], RDFS.label, Literal(k['@surfaceForm'], lang='en')))
            graph.add((course_name_space[link_name], RDFS.comment, Literal(k['@surfaceForm'], lang='en')))
            graph.add((course_name_space[link_name], FOAF.name, Literal(k['@surfaceForm'], lang='en')))
            graph.add((course_name_space[link_name], property_name_space.partOf, course))
    except:
        time.sleep(randrange(1,5))
        print("data is not defined")
    
def parse_course_details(row, graph, json_data):
    #print(row[0])
    course = course_name_space[row[0]]
    graph.add((course, RDF.type, course_name_space.Course))
    graph.add((course, FOAF.name, Literal(row[0])))
    graph.add((course, RDFS.label, Literal(row[1], lang='en')))
    graph.add((course, property_name_space.CourseSubject, Literal(row[2], lang='en')))
    graph.add((course, property_name_space.CourseNumber, Literal(row[3])))
    graph.add((course, RDFS.comment, Literal(row[4], lang='en')))
    graph.add((course, dbo.abstract, Literal(row[4], lang='en')))
    graph.add((course, RDFS.seeAlso, URIRef(row[5])))
    graph.add((course, property_name_space.CourseName, Literal(row[1], lang='en')))
    graph.add((course, property_name_space.courseAt, university_name_space.ConcordiaUniversity))
    graph.add((university_name_space.ConcordiaUniversity, property_name_space.hasCourse, course))
    
    add_description_to_dbpedia(graph, course, ("%s,%s"%(row[1], row[4])).strip(), json_data, row[0])
    

def main():
    g = Graph()
    #g.add((property_name_space['studies'], RDF.type, RDF.Property))
    file = pathlib.Path("description_data.json")
    if file.exists():
        with open("description_data.json", "r+", encoding='utf-8') as json_data_file:
            json_data = json.load(json_data_file)
    else:
        json_data = {}
            
    try:
        with open("CourseList/Course_ConcordiaUniversity_WithURL.csv", "r+", encoding='utf-8') as csvfile:
            course_details = csv.reader(csvfile, delimiter=',', quotechar='"')
            for row in course_details:
               parse_course_details(row, g, json_data)
    finally:
        with open("description_data.json", "w", encoding='utf-8') as json_data_file:
            json.dump(json_data, json_data_file, ensure_ascii=False, indent=4)
    
    with open("student_details.csv", "r+") as csvfile:
        student_details = csv.reader(csvfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        for row in student_details:
            parse_student_add_to_graph(row, g)

    #print(g.serialize(format='turtle'))
    with open("G12.ttl", "wb") as f:
        f.write(g.serialize(format='turtle'))
        f.write(b'''people:Student rdf:type rdfs:Class ;
rdfs:label "Student"@en ;
rdfs:comment "Person studies at some university"@en ;
rdfs:subClassOf foaf:Person ;
owl:sameAs dbpedia:Student .

course:Course rdf:type rdfs:Class ;
rdfs:label "Course Name"@en ;
rdfs:comment "Subject taught in university"@en ;
rdfs:subClassOf dbo:TopicalConcept ;
owl:sameAs dbpedia:AcademicSubject.

people:StudentRecord rdf:type rdfs:Class ;
rdfs:label "Student Course Details"@en ;
rdfs:comment "Student Course, Semester and Grade details" ;
owl:sameAs <http://dbpedia.org/resource/Transcript_(education)> .

property:Grade rdf:type rdf:Property ;
rdfs:label "Grade"@en ;
rdfs:comment "Grade of course obtained by Student"@en ;
rdfs:domain people:StudentRecord ;
rdfs:range rdfs:Literal .

property:Semester rdf:type rdf:Property ;
rdfs:label "Semester"@en ;
rdfs:comment "Semester in which course has taken by the Student"@en ;
rdfs:domain people:StudentRecord ;
rdfs:range rdfs:Literal .

property:Course rdf:type rdf:Property ;
rdfs:label "Course"@en ;
rdfs:comment "Course obtained by Student"@en ;
rdfs:domain people:StudentRecord ;
rdfs:range course:Course .

property:hasRecord rdf:type rdf:Property ;
rdfs:label "Student Record"@en ;
rdfs:comment "Record of the course details of the Student"@en ;
rdfs:domain people:Student ;
rdfs:range people:StudentRecord .

property:CourseName rdf:type rdf:Property ;
rdfs:label "Concadination of course subject and course id"@en ;
rdfs:comment "Concadination of course subject and course id"@en ;
rdfs:domain course:Course ;
rdfs:range rdfs:Literal .

uni:University rdf:type rdfs:Class ;
rdfs:label "University"@en ;
rdfs:comment "Education Institiution" ;
owl:sameAs <http://dbpedia.org/resource/University> .

uni:ConcordiaUniversity rdf:type uni:University ;
rdfs:label "Concordia University"@en ;
rdfs:comment "Concordia University, Montreal"@en ;
foaf:name "Concordia University" ;
owl:sameAs <http://dbpedia.org/resource/Concordia_University> ;
rdfs:seeAlso <http://dbpedia.org/resource/Concordia_University> .

property:studiesAt rdf:type rdf:Property ;
rdfs:label "Student Studies"@en ;
rdfs:comment "where student studies"@en;
rdfs:domain people:Student ;
rdfs:range uni:University .

property:hasCourse rdf:type rdf:Property ;
rdfs:label "Course Available"@en ;
rdfs:comment "Course Available At Univesity"@en ;
rdfs:domain uni:University ;
rdfs:range course:Course ;
owl:inverseOf property:courseAt .

property:courseAt rdf:type rdf:Property ;
rdfs:label "Course At"@en ;
rdfs:comment "Course At"@en ;
rdfs:domain course:Course ;
rdfs:range uni:University ;
owl:inverseOf property:hasCourse .

property:isStudentOf rdf:type rdf:Property ;
rdfs:label "Student of university"@en ;
rdfs:comment "Student of university"@en ;
rdfs:domain people:Student ;
rdfs:range uni:University ;
owl:inverseOf property:hasStudent .

property:hasStudent rdf:type rdf:Property ;
rdfs:label "Student of university"@en ;
rdfs:comment "Student of university"@en ;
rdfs:domain uni:University ;
rdfs:range people:Student ;
owl:inverseOf property:isStudentOf .

property:student_id rdf:type rdf:Property ;
rdfs:label "Student ID"@en ;
rdfs:comment "Student ID"@en ;
rdfs:domain people:Student ;
rdfs:range rdfs:Literal .

course:Topic rdf:type rdfs:Class ;
rdfs:label "Topic"@en ;
rdfs:comment "Topic"@en ;
owl:sameAs <http://dbpedia.org/resource/Topic> .

property:partOf rdf:type rdf:Property ;
rdfs:label "Part Of"@en ;
rdfs:comment "Part Of which course"@en ;
rdfs:domain course:Topic ;
rdfs:range course:Course .
''')

if __name__ == '__main__':
    print("Started")
    main()
    print("Finished Execution")