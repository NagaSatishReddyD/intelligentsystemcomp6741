# -*- coding: utf-8 -*-
"""
@author: Reethu Navale and Naga Satish
"""

import rdflib


g = rdflib.Graph()

g.parse("G12.ttl", format="turtle")

########### SPARQL query for student's completed courses and its grade
studentId = '40104630'
print("Student Id :" + studentId +"\nList of the courses :")
print("Student Name\t\t\tCourseId\t\t\tGrade\t\t\tSemester\t\t\tCourseName")
print("---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------")
q4 = g.query(
        f""" SELECT ?firstname ?courseid ?grade ?semester ?coursename
        WHERE {{
        ?stu rdf:type people:Student .
        ?stu property:student_id '{studentId}' .
        ?stu foaf:givenName ?firstname .
        ?stu property:hasRecord ?record .
        {{
        SELECT ?courseid ?coursename ?grade ?semester
        WHERE{{
        ?record property:Course ?courseuri .
        ?courseuri foaf:name ?courseid .
        ?courseuri property:CourseName ?coursename .
        FILTER(lang(?coursename)='en')
        ?record property:Grade ?grade .
        ?record property:Semester ?semester .
        }}
        }}      
        }}
        """)

for row in q4:
    print("%s\t\t\t%s\t\t\t%s\t\t\t%s\t\t\t%s" % row)

studentName = 'NagaSatish'
print("Student Name :" + studentName)
print("CourseId\t\t\tGrade\t\t\tSemester\t\t\tCourseName")
print("---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------")
q4a = g.query(
        f""" SELECT ?courseid ?grade ?semester ?coursename 
        WHERE {{
        ?stu rdf:type people:Student .
        ?stu foaf:givenName ?stuname .        
        ?stu property:hasRecord ?record .
        {{
        SELECT ?courseid ?coursename ?grade ?semester
        WHERE{{
        ?record property:Course ?courseuri .
        ?courseuri foaf:name ?courseid .
        ?courseuri property:CourseName ?coursename .
        FILTER(lang(?coursename)='en')
        ?record property:Grade ?grade .
        ?record property:Semester ?semester .
        }}
        }}
        FILTER (regex(str(?stuname),'{studentName}','i'))
        }}
        """)

for row in q4a:
    print("%s\t\t\t%s\t\t\t%s\t\t\t%s" % row)