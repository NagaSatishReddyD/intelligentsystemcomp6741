import requests
from bs4 import BeautifulSoup
import csv

page = requests.get('https://www.concordia.ca/academics/graduate/calendar/current/encs/engineering-courses.html')
url = str('https://www.concordia.ca/academics/graduate/calendar/current/fasc/biol-phd.html')
soup = BeautifulSoup(page.content,'html.parser')
course_details = soup.find(id='content-main')
#print(course_details)

course_list = course_details.find_all(class_='wysiwyg parbase section')
#print(course_list)
c_details = course_list[2].findAll(class_='large-text')
#print(c_details)
TagCount = len(c_details) - 2
index = 2
#print(TagCount)
for c_d in range(TagCount):
    c1 = c_details[index].get_text()
    #print(c1)
    ll = c1.splitlines()
    #print(ll)
    lineindex=0
    Name = ll[lineindex].split('(')[0]
    #print(Name)
    cd = ll[1:]
    CourseDesc = ''.join(cd).strip()
    #print(CourseDesc)
    pos = Name.index(' ')
    CourseSubject = Name.split(' ')[0]
    CourseNumber = Name.split(' ')[1]
    tempName = Name.partition(' ')[2]
    CourseName = tempName.partition(' ')[2].strip()
    CourseId = CourseSubject+CourseNumber
    print("Course Id : " + CourseId)
    print("Course Name : " + CourseName)
    print("Course Subject : " + CourseSubject)
    print("Course Number : " + CourseNumber)    
    print("Course Description : " + ''.join(CourseDesc))
    print()
    
    with open('C:/MySoftware/temp.csv',mode='a', encoding='utf-8', newline='') as cs_course_file:
        course_writer = csv.writer(cs_course_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)
        course_writer.writerow([CourseId, CourseName, CourseSubject,CourseNumber,CourseDesc,url])
       
    index=index+1
    
with open('C:/MySoftware/temp.csv', 'r') as infile, open('C:/MySoftware/GDBA6_Course_WithURL.csv', 'w') as outfile:
    temp = infile.read().replace("['", "").replace("']", "").replace("[]", "").replace("', '", "")
    outfile.write(temp)