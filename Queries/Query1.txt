# -*- coding: utf-8 -*-
"""
@author: Reethu Navale and Naga Satish
"""

import rdflib


g = rdflib.Graph()

g.parse("G12.ttl", format="turtle")

### This will give the number of triples in non query form
#print(len(g)) 

########### SPARQL query for triple count
q1 = g.query(
    """SELECT (COUNT (*) as ?CountTriples)
      WHERE {
          ?s ?p ?o .
       }""" )


for row in q1:
    print("Total number of triples in the RDF : %s" % row)
